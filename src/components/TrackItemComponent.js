import React from 'react';

export default function TrackList(props) {
    return (
        <a
            className={`trackItem ${props.currentTrack === props.id ? 'active' : ''}`}
            onClick={ () => props.handleClick(props.id) }
        >
            <span className='trackItem__id'>{props.id}</span>
            <p className='trackItem__name'>{props.name}</p>
        </a>
    )
}
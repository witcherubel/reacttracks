import React, {Component} from 'react';
import axios from 'axios';

import TrackItemComponent from './../components/TrackItemComponent';

export default class TrackListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTrack: null,
            tracks: [],
        }
    }

    componentDidMount() {
        axios.get(`https://cors-anywhere.herokuapp.com/https://api.deezer.com/search/track?q=eminem`)
            .then(res => {
                console.log(res.data);
                const tracks = res.data.data;
                this.setState({ tracks });
            });
    }

    render() {
        return (
            <section className='trackList'>
                <div className='trackList__head'>
                    <h3>Title</h3>
                    <p>Subtitle</p>
                </div>
                <div className='trackList__wrapper'>
                    {this.getAllTracks()}
                </div>
            </section>
        );
    }

    getAllTracks() {
        return this.state.tracks.map(
            (item) =>
                <TrackItemComponent
                    key={item.id}
                    id={item.id}
                    name={item.title}
                    handleClick={ (id) => this.handleClick(id)}
                    {...this.state}
                />
        );
    }

    handleClick(id) {
        this.setState({
            currentTrack: id,
        });
    }
}
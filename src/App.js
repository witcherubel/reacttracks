import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TrackListContainer from "./containers/TrackList";

class App extends Component {
  render() {
    return (
      <div className="App">
          <TrackListContainer />
      </div>
    );
  }
}

export default App;
